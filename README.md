# CODE_EXAM_UNAMUR2018

1) Exploratory

Contains:
- Description of the database
- Graphics
- Probabilities

2) Class_Creation

Contains:
- Creation of the different classes

3) Simulation

First part:
- Generation of the probabilities + storage in a table
- Generation of the new dataframe + csv file
- Simulation
- Additional plots for comparison

NB: We do not succeed to take into account prices + the code is very long to run

4) Additional questions

- Calculation of the returning historic
- Buying historic (do not work)