from random import randint


# Definition of the different classes

# Class object : Customer
class Customer(object):
    def __init__(self, ID, Person, Budget=0):
        self.ID = ID
        self.Budget = Budget
        self.Person = Person


# Customers that come several times
class Returning(Customer):
    def giveReturningBudget(self):
        if self.Person == "regular":
            self.Budget = 250
        else:
            self.Budget = 500

        return self.Budget


# Customers that come only one time
class OneTime(Customer):
    def giveOneTimeBudget(self):
        self.Budget = 100
        if self.Person == "TripAdvisor":
            self.Budget = self.Budget - randint(1, 10)
        return self.Budget
