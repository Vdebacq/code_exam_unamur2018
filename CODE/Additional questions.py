from CODE.Simulations import *
import collections

# How many returning customers

dico_counts = collections.Counter(Coffeebar['CUSTOMER'].value_counts().tolist())
number_returning_customer = len(Coffeebar['CUSTOMER'].value_counts().tolist()) - dico_counts[1]


# Buying historic
def buying_historic(TIME):
    for value in Coffeebar_simulation['CUSTOMER']:
        if value in tab_returning:
            sentence = "At %s, the customer " % (str(TIME))
            sentence += (str(value)) + " has bought " + (str(Coffeebar_simulation['FOOD'])) + ',' + (
                str(Coffeebar_simulation['DRINKS']))
        print(sentence)


buying_historic("08:00:00")
