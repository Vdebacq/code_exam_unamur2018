import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.rcdefaults()
import matplotlib.pyplot as plt


# Importing database as dataframe named : Coffeebar

path = "./Data/"

Coffeebar = pd.read_csv(path + "Coffeebar_2013-2017.csv", sep=';')

# DRINKS sold

print(Coffeebar['DRINKS'].unique())

# FOOD sold

print(Coffeebar['FOOD'].unique())

# Number of unique customers

print(Coffeebar['CUSTOMER'].nunique())

# Plots

# Plot1 : Drinks

# Quantity of each type of drink
Ndrinks = dict()
for value in Coffeebar['DRINKS']:
    if value in Ndrinks:
        Ndrinks[value] += 1
    else:
        Ndrinks[value] = 1
print(Ndrinks)

# Plot for drink
Drink = ('frappucino', 'soda', 'coffee', 'tea', 'water', 'milkshake')
y_pos = np.arange(len(Drink))
Quantity = (40436, 95583, 53833, 40556, 40915, 40752)

plt.bar(y_pos, Quantity, align='center', alpha=0.5)
plt.xticks(y_pos, Drink)
plt.ylabel('Quantity sold for 5 years')
plt.title('Type of drink')
plt.show()

# Plot2 : Foods

# Quantity of each type of food
Nfood = dict()
for value in Coffeebar['FOOD']:
    if value in Nfood:
        Nfood[value] += 1
    else:
        Nfood[value] = 1
print(Nfood)

# Plot for food
Food = ('nothing', 'sandwich', 'pie', 'muffin', 'cookie')
y_pos = np.arange(len(Food))
Quantity = (147805, 68623, 32010, 31785, 31852)

plt.bar(y_pos, Quantity, align='center', alpha=0.5)
plt.xticks(y_pos, Food)
plt.ylabel('Quantity sold for 5 years')
plt.title('Type of food')
plt.show()

# Probability definition

Coffeebar['DATE'], Coffeebar['HOUR'] = Coffeebar['TIME'].str.split(' ', ).str
Food = Coffeebar[["HOUR", "FOOD"]]


# Quantity of each type of food for a precise time
def Qfoodh(hour, FOOD):
    compteur = 0
    for i, value in enumerate(Food['HOUR']):
        if value == hour:
            if Food['FOOD'][i] == FOOD:
                compteur = compteur + 1
    return compteur


# Quantity of food for a precise time
def Qfood(hour):
    compteur = 0
    for value in Food['HOUR']:
        if value == hour:
            compteur += 1
    return compteur


# Calculation of the probability
def ProbaFood(hour, FOOD):
    resultat = (Qfoodh(hour, FOOD) / Qfood(hour)) * 100
    return resultat


Drinks = Coffeebar[["HOUR", "DRINKS"]]


# Quantity of each type of drink for a precise time
def Qdrinkh(hour, DRINKS):
    compteur = 0
    for i, value in enumerate(Drinks['HOUR']):
        if value == hour:
            if Drinks['DRINKS'][i] == DRINKS:
                compteur = compteur + 1
    return compteur


# Quantity of drink for a precise time
def Qdrink(hour):
    compteur = 0
    for value in Drinks['HOUR']:
        if value == hour:
            compteur += 1
    return compteur


# Calculation of the probability
def ProbaDrink(hour, DRINKS):
    resultat = (Qdrinkh(hour, DRINKS) / Qdrink(hour)) * 100
    return resultat
