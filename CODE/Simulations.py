from CODE.Exploratory import *
from CODE.Class_Creation import *
from random import randint
import matplotlib.pyplot as plt
import time
import sys


# Creation of a tab to check if all the ID are different
def verif_id_one_time():
    while True:
        id = "CID" + str(randint(10000000, 99999999))
        if id in tab_returning:
            continue
        elif id in tab_one_time:
            continue
        else:
            tab_one_time.append(id)
            break
    return id


# Same for the returning
def verif_id_returning():
    resultat = ""
    while True:
        if len(tab_returning) < 1000:
            id = "CID" + str(randint(10000000, 99999999))
            if id in tab_returning:
                continue
            elif id in tab_one_time:
                continue
            else:
                tab_returning.append(id)
                resultat = id
                break
        else:
            nbr = randint(0, 999)
            resultat = tab_returning[nbr]
            break
    return resultat


# Preparation of the output file (csv)
def generate_output_file():
    columns = ['TIME', 'CUSTOMER', 'DRINKS', 'FOOD']


# Probabilities definition for each hour

def NombreUniqueDrink():
    return Coffeebar['DRINKS'].unique()


def NombreUniqueFood():
    return Coffeebar['FOOD'].unique()


# For drinks
def generate_proba_drink():
    global proba_drink
    global drink
    start_day = 1490166000
    drink = NombreUniqueDrink()
    cpt = 0
    while cpt <= 170:
        if cpt < 36:
            heure = time.strftime("%H:%M:%S", time.localtime(start_day))
            tab2 = []
            for i in drink:
                tab2.append(int(ProbaDrink(heure, i) * 100))  # Multiplied by 100 to be more clear
            proba_drink.append(tab2)
            sys.stdout.write("[%.2f /100 en cours...]\r" % float(cpt / 170 * 100))  # sys is like a print
            sys.stdout.flush()  # With the "flush" : Display the printing element then deletes it just after

            start_day = start_day + 300  # 300 seconds = 5 minutes
            cpt = cpt + 1
        elif cpt < 96:
            heure = time.strftime("%H:%M:%S", time.localtime(start_day))
            tab2 = []
            for i in drink:
                tab2.append(int(ProbaDrink(heure, i) * 100))
            proba_drink.append(tab2)
            sys.stdout.write("[%.2f /100 en cours...]\r" % float(cpt / 170 * 100))
            sys.stdout.flush()

            start_day = start_day + 120  # 120 seconds = 2 minutes (like in the database)
            cpt = cpt + 1

        else:
            heure = time.strftime("%H:%M:%S", time.localtime(start_day))
            tab2 = []
            for i in drink:
                tab2.append(int(ProbaDrink(heure, i) * 100))
            proba_drink.append(tab2)
            sys.stdout.write("[%.2f /100 en cours...]\r" % float(cpt / 170 * 100))
            sys.stdout.flush()

            start_day = start_day + 240  # 140 seconds = 4 minutes
            cpt = cpt + 1


# For food
def generate_proba_food():
    global proba_food
    global food
    start_day = 1490166000
    food = NombreUniqueFood()
    cpt = 0
    while cpt <= 170:
        if cpt < 36:
            heure = time.strftime("%H:%M:%S", time.localtime(start_day))
            tab2 = []
            for i in food:
                tab2.append(int(ProbaFood(heure, i) * 100))
            proba_food.append(tab2)
            sys.stdout.write("[%.2f /100 en cours...]\r" % float(cpt / 170 * 100))
            sys.stdout.flush()

            start_day = start_day + 300  # 300 seconds = 5 minutes
            cpt = cpt + 1
        elif cpt < 96:
            heure = time.strftime("%H:%M:%S", time.localtime(start_day))
            tab2 = []
            for i in food:
                tab2.append(int(ProbaFood(heure, i) * 100))
            proba_food.append(tab2)
            sys.stdout.write("[%.2f /100 en cours...]\r" % float(cpt / 170 * 100))
            sys.stdout.flush()

            start_day = start_day + 120  # 120 seconds = 2 minutes (like in the database)
            cpt = cpt + 1

        else:
            heure = time.strftime("%H:%M:%S", time.localtime(start_day))
            tab2 = []
            for i in food:
                tab2.append(int(ProbaFood(heure, i) * 100))
            proba_food.append(tab2)
            sys.stdout.write("[%.2f /100 en cours...]\r" % float(cpt / 170 * 100))
            sys.stdout.flush()

            start_day = start_day + 240  # 140 seconds = 4 minutes
            cpt = cpt + 1


# Putting the probabilities in a table

# For drinks
def calcul_proba_drink(j):
    global proba_drink
    global drink
    somme = 0
    for i in proba_drink[j]:
        somme = somme + i
    if somme == 0:
        return " "
    else:
        aleatoire = randint(1, somme)
        cas = 0
        for l in range(len(drink)):
            cas = cas + proba_drink[j][l]
            if aleatoire <= cas:
                return drink[l]


# For food
def calcul_proba_food(j):
    global proba_food
    global food
    somme = 0
    for i in proba_food[j]:
        somme = somme + i

    if somme == 0:
        return " "
    else:
        if somme > 9500:
            aleatoire = randint(1, somme)
            cas = 0
            for l in range(len(food)):
                cas = cas + proba_food[j][l]
                if aleatoire <= cas:
                    return food[l]
        else:
            aleatoire = randint(1, 10000)
            cas = 0
            for l in range(len(food)):
                cas = cas + proba_food[j][l]
                if aleatoire <= cas:
                    return food[l]
            return " "


proba_drink = []
drink = []
proba_food = []
food = []

# Generation of the csv table

columns = ['TIME', 'CUSTOMER', 'DRINKS', 'FOOD']

# List of all the returning ID
tab_returning = []

# List of all the one time ID
tab_one_time = []

# Compteur for money
cpt = 0

# Beginning of the day : 08:00:00
j = 0
start_day = 1490166000

# What will be printed in the consol to know what the program is doing
jour = 0
resultat = ""
fichier = input("Enter the name of the file (ex: output.csv) : ")

print("Calculation of the drink stats...")
generate_proba_drink()
print("Calculation of the food stats...")
generate_proba_food()

print("Beginning of the simulation for 1825 days")

# Code for the simulation

tableau_heure = []
tableau_customer = []
tableau_drinks = []
tableau_foods = []
while jour < 1825:
    drinks = calcul_proba_drink(j)
    foods = calcul_proba_food(j)
    sys.stdout.write(str(jour) + " jours\r")
    sys.stdout.flush()
    aleatoire = randint(1, 1000)
    if aleatoire <= 200:
        if aleatoire <= 67:
            r = Returning(111, "hipsters")
            cpt += r.giveReturningBudget()
            resultat = verif_id_returning()
        else:
            r = Returning(111, "regular")
            cpt += r.giveReturningBudget()
            resultat = verif_id_returning()
    else:
        if aleatoire <= 720:
            o = OneTime(111, "regular ")
            cpt += o.giveOneTimeBudget()
            resultat = verif_id_one_time()
        else:
            o = OneTime(111, "TripAdvisor")
            cpt += o.giveOneTimeBudget()
            resultat = verif_id_one_time()
    if j < 36:
        hour_human = time.strftime(" %H:%M:%S", time.localtime(start_day))
        start_day = start_day + 300
        j += 1
    elif j < 96:
        hour_human = time.strftime(" %H:%M:%S", time.localtime(start_day))
        start_day = start_day + 120
        j += 1
    elif j < 170:
        hour_human = time.strftime(" %H:%M:%S", time.localtime(start_day))
        start_day = start_day + 240
        j += 1
    else:
        j = 0
        jour += 1
        start_day = 1490166000
    # Adding the values in different tables which will be used to create the output file (csv)
    tableau_heure.append(hour_human)
    tableau_customer.append(resultat)
    tableau_drinks.append(drinks)
    tableau_foods.append(foods)

# Matching all the tables in a csv file with pandas
print("Generation of the report...")
raw_data = {"TIME": tableau_heure,
            "CUSTOMER": tableau_customer,
            "DRINKS": tableau_drinks,
            "FOOD": tableau_foods}
Coffeebar_simulation = pd.DataFrame(raw_data, columns=columns)
Coffeebar_simulation.to_csv(fichier)

print("Simulation done")
print("Money earned: " + str(cpt))


# Dataframe comparison

def Make_a_plot(Data_Frame, column, title):
    values = Data_Frame[column].value_counts().keys().tolist()
    counts = Data_Frame[column].value_counts().tolist()

    fig, ax = plt.subplots()
    ax.bar(values, counts)
    ax.set_title(title)
    plt.show()


Make_a_plot(Coffeebar, "DRINKS", "Total amount of drink sold")

Make_a_plot(Coffeebar_simulation, "DRINKS", "Total amount of drink sold for the simulation")

Make_a_plot(Coffeebar, "FOOD", "Total amount of food sold")

Make_a_plot(Coffeebar, "FOOD", "Total amount of food sold for the simulation")
